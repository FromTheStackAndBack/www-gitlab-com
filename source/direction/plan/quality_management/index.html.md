---
layout: markdown_page
title: "Category Direction - Quality Management"
description: View the GitLab category strategy for Requirements Management, part of the Plan stage's Requirements Management group.
canonical_path: "/direction/plan/quality_management/"
---

- TOC
{:toc}

## Quality Management
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

|                       |                                 |
| -                     | -                               |
| Stage                 | [Plan](/direction/plan/)        |
| Maturity              | [Planned](/direction/maturity/) |
| Content Last Reviewed | `2020-03-27`                    |

### Introduction and how you can help

Welcome to the category strategy for Quality Management, part of the Plan stage's [Certify](/handbook/product/product-categories/#certify-group) group. To provide feedback or ask questions, please reach out to the group's Product Manager, Mark Wood ([E-mail](mailto:mwood@gitlab.com)).

We believe in a world where **everyone can contribute**. We value your contributions, so here are some ways to join in!

* Please comment and/or vote on any of the Quality Management
[issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AQuality%20Management) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AQuality%20Management)!
* Feel free to share feedback directly via email, Twitter or on a video call.
* Don't hesitate to create an issue for a new feature or enhancement, if one does not already exist.
* Finally, please create an MR against this direction page to make it better!

### Overview

Many organizations manage quality through both manual and automated testing. This
testing is supported by artifacts such as test plans, test cases, and test execution runs.
These artifacts are then integrated back with documented issues and feature requirements,
allowing for feature-test traceability and quantifying coverage. Quality management
in GitLab addresses these problems and use cases.

We have performed a Solution Validation for the [Quality Management MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/208306).


## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

The first step in building out Quality Management is a scaffolding framework for testing. In particular, we are calling these test cases, test suites, and test sessions. These will be first class native objects in GitLab, used to track the quality process of testing itself.

The MVC can be seen at [https://gitlab.com/gitlab-org/gitlab/-/issues/216149](https://gitlab.com/gitlab-org/gitlab/-/issues/216149). At present, we're hoping to begin work on the MVC in GitLab 13.3.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Competitors in this space include qTest, Test Rail, and HPQC (HP Quality Center). They are focused on managing test cases as part of the software development lifecycle. Our approach and response will be to have similar basic test case management features (i.e. test objects), and then quickly move horizontally to integrate with other places in GitLab, such as issues and epics and even requirements management. See https://gitlab.com/groups/gitlab-org/-/epics/670. With this strategy, we would not be necessarily competing directly with these existing incumbents, but helping users with the integration pains of multiple tools and leveraging other, more mature areas of GitLab as we iterate.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We have yet to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

Few GitLab customers or prospects have asked about Quality Management. But the few that have, are asking about best practices and how they can possibly use GitLab and not worry with another tool. We are considering [https://gitlab.com/groups/gitlab-org/-/epics/617](https://gitlab.com/groups/gitlab-org/-/epics/617) for that very purpose.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

| Issue                                                                                                                          | 👍 |
| -                                                                                                                              | -  |
| [GitLab Quality Center](https://gitlab.com/gitlab-org/gitlab/issues/1538)                                                      | 36 |
| [Test case management and test tracking in a Native Continuous Delivery way](https://gitlab.com/gitlab-org/gitlab/issues/8766) | 10 |
| [Test cases and test suites](https://gitlab.com/gitlab-org/gitlab/issues/26097)                                                |  8 |


## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

We continue to work with GitLab's Quality Team to scope out the MVC of quality management. See [https://gitlab.com/gitlab-org/gitlab/-/issues/216149](hhttps://gitlab.com/gitlab-org/gitlab/-/issues/216149).

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->
The top Vision Item for this category is to release the [MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/216149). It is our goal that by performing careful solution validation, we will come out with an initial offering for this product category which matches well with our target customers.
