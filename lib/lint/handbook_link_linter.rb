require 'colorize'

module Lint
  class HandbookLinkLinter
    HANDBOOK_DIRECTORY = File.join(File.expand_path('../..', __dir__), 'sites/handbook/source/')
    EXCEPTIONS = [
      '/handbook/engineering/projects',
      '/handbook/product/product-categories',
      '/handbook/business-ops/order-processing',
      '/handbook/marketing/product-marketing/competitive/compete',
      '/handbook/hiring/charts/sales'
    ].freeze

    # TODO: delete entries from this list and fix detected failures
    TEMPORARY_IGNORE = [
      '/handbook/business-ops',
      '/handbook/customer-success',
      '/handbook/engineering/infrastructure',
      '/handbook/finance',
      '/handbook/hiring',
      '/handbook/marketing/blog',
      '/handbook/marketing/campaigns',
      '/handbook/marketing/community-relations',
      '/handbook/marketing/corporate-marketing',
      '/handbook/marketing/events',
      '/handbook/marketing/growth-marketing',
      '/handbook/marketing/marketing-operations',
      '/handbook/marketing/product-marketing',
      '/handbook/marketing/revenue-marketing',
      '/handbook/marketing/technical-evangelism',
      '/handbook/on-call',
      '/handbook/people-group',
      '/handbook/resellers',
      '/handbook/sales',
      '/handbook/security',
      '/handbook/support',
      '/handbook/tax/performance-indicators'
    ].freeze

    class LinterError
      def initialize(line, description: nil)
        @line = line
        @description = description
      end

      def +(other)
        puts
        puts
        puts @description
        @line += other
      end
    end

    attr_reader :errors

    def initialize(links)
      @links = links
      @errors = []
    end

    def check
      links.each do |element|
        link = element.attr['href']

        attributes = extract_link_attributes(link)

        partial_filename = File.basename(attributes[:path])
        relative_dirname = File.dirname(attributes[:path])
        absolute_dirname = File.join(HANDBOOK_DIRECTORY, relative_dirname)

        next if redirect?(relative_dirname)
        next if exception?(relative_dirname)
        next if temporary_ignore?(relative_dirname)

        unless File.directory?(absolute_dirname)
          errors << LinterError.new(element.options[:location], description: "Cannot find directory #{absolute_dirname}".red)
          next
        end

        filename = Dir.children(absolute_dirname).find { |file| file.include?(partial_filename) }

        unless filename
          errors << LinterError.new(element.options[:location], description: "Cannot find #{partial_filename} in #{absolute_dirname}".red)
          next
        end

        file_path = File.join(absolute_dirname, filename)

        unless File.file?(file_path)
          errors << LinterError.new(element.options[:location], description: "#{file_path} does not exist".red)
          next
        end

        next if unsupported_file?(file_path)

        unless correct_anchor?(file_path, attributes[:anchor])
          errors << LinterError.new(element.options[:location], description: "Anchor #{attributes[:anchor]} cannot be found in #{file_path}".red)
          next
        end
      end
    end

    private

    attr_reader :links

    def correct_anchor?(path_to_file, anchor)
      return true unless anchor

      anchors = AnchorExtractor.instance.fetch(path_to_file)

      anchors.include?(anchor)
    end

    def redirect?(source)
      redirects.any? { |redirect| redirect.start_with?(source) || source.start_with?(redirect) }
    end

    def exception?(source)
      EXCEPTIONS.include?(source)
    end

    def temporary_ignore?(source)
      TEMPORARY_IGNORE.any? { |ignore| source.start_with?(ignore) }
    end

    def unsupported_file?(file_path)
      file_path.end_with?('.erb', '.haml')
    end

    def extract_link_attributes(href)
      path, anchor = href.split('#')

      path = File.join(path, 'index.html') if File.extname(path).empty?

      { path: path, anchor: anchor }
    end

    def redirects
      @redirects ||= YAML.load_file('data/redirects.yml').map { |redirect| redirect['sources'] }.flatten.uniq
    end
  end

  class AnchorExtractor
    PUNCTUATION_REGEXP = /[^\p{Word}\- ]/u
    include Singleton

    def initialize
      @anchors_for_file = {}
      super
    end

    def fetch(filepath)
      return anchors_for_file[filepath] if anchors_for_file.key?(filepath)

      doc = MarkdownLint::Doc.new(File.read(filepath))

      used_ids = {}

      anchors = doc.find_type_elements(:header).map do |header|
        header.attr.key?('id') ? header.attr['id'] : string_to_anchor(doc.extract_text(header).first, used_ids)
      end

      anchors += doc.find_type_elements(:li).map { |li| li.attr['id'] }.compact

      anchors_for_file[filepath] = anchors

      anchors
    end

    private

    attr_reader :anchors_for_file

    def extract_text_from_link(string)
      result = string.match(/\[(.+?)\]\(.+\)/)

      return result[1] if result

      string
    end

    def string_to_anchor(string, used_ids)
      anchor = extract_text_from_link(string)
                 .strip
                 .downcase
                 .gsub(PUNCTUATION_REGEXP, '') # remove punctuation
                 .gsub(/^\W+/, '')
                 .tr(' ', '-') # replace spaces with dash

      if used_ids.key?(anchor)
        used_ids[anchor] += 1
        anchor = "#{anchor}-#{used_ids[anchor]}"
      else
        used_ids[anchor] = 0
      end

      anchor
    end
  end
end

rule "LINK", "Link linter failure" do
  tags :links
  check do |doc|
    handbook_links = doc.find_type_elements(:a).select { |link| link.attr['href'].start_with?('/handbook') }

    linter = Lint::HandbookLinkLinter.new(handbook_links)
    linter.check

    linter.errors
  end
end
