---
layout: handbook-page-toc
title: "Professional Services Offerings Framework"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Hierarchy
1. **Levels of service**: There are three levels of service that represent the different services buyer personas.
1. **Categories**: Each level has one or many categories of service that define a set of related services either by type or product area.
1. **Offerings**: Each category has one or many offerings that is a single consumable unit that has all of the required pieces to make a complete service offering.
1. **Offering Variants**: Each offering may have one or many variants that allow it to be deployed in different ways.

Each offering at least has a variant called “standard” or “custom” to define if it can be delivered with a standard SKU / out of the box SOW. For example, enterprise versus commercial or remote versus on-site or one-time versus with an embedded engineer. See Professional Services Full Catalog for a detailed listing of offerings.

### Offering maturity model

The services maturity framework provides for 5 maturity levels for offerings: planned, minimal, viable, complete and lovable.

* **Planned**: A future planned offering
* **Minimal**: The offering is defined, a vision for moving to complete exists
* **Viable**: We have delivered the offering at least once, feeding lessons learned into the completion plan. At least some marketing materials and execution plans from Complete
* **Complete**: An offering that can be consistently delivered: predictability in timing, results, and margin. 
* **Lovable**: The offering is at full maturity, positive NPS & impact on customer’s adoption of GitLab product

### Categories of service

GitLab provides several categories of services to ensure a full suite of offerings for customers.

* Implementation Services
* Integration Services
* Migration Services
* Education Services
* Dedicated Services
* Expert Services

There are a few different offerings in each category. The tables below show the offerings in each category and their level of maturity.

#### Implementation Services

| Offering | Maturity |
| :--      | :--:     |
| Rapid Results implementation | Planned |
| Custom implementation | Complete |
| Assessment and optimization | Planned |

#### Integration Services

| Offering | Maturity |
|:---------|:--------:|
| LDAP, SAML, SSO | Complete |
| JIRA | Complete |
| Jenkins | Complete |

#### Migration Services

| Offering | Maturity |
|:---------|:--------:|
| Migration from GitHub, Bitbucket, SVN, etc. | Complete |
| Migration to GitLab.com | Planned |

#### Education Services

| Offering | Maturity |
|:---------|:--------:|
| Live Instructor-Led training (remote and onsite) | Viable |
| Asynchronous eLearning  | Minimal |
| Certification assessments  | Minimal |

#### Dedicated Services

| Offering | Maturity |
|:---------|:--------:|
| Dedicated remote engineer (3, 6, or 12 months) | Complete |

#### Expert Services

| Offering | Maturity |
|:---------|:--------:|
| Maturity model consulting | Planned |
| InnerSource consulting  | Planned |
| Measuring value of DevOps  | Planned |
