---
layout: markdown_page
title: "GitLab vs GitHub CI-CD Roadmap Comparison"
description: "Compare the CI-CD capability as documented in GitHub roadmap with similar capability in GitLab."
canonical_path: "/devops-tools/github/GitLab-vs-GitHub-CI-CD-Roadmap-Comparison.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


# Background

On July 28, 2020 [GitHub announced their roadmap will be made public](https://github.blog/2020-07-28-announcing-the-github-public-roadmap/).  GitLab welcomes this move by GitHub since it ensures greater transparency and enables customers to more easily compare the product direction and potential benefits to their DevOps processes.  

GitLab has provided CI-CD capability for several years, as a result several of the roadmap capabilities planned by GitHub are already available at GitLab.  

This page highlights two aspects:

-  Compares the CI-CD capability as documented in GitHub roadmap with similar capability in GitLab.  It also shows dates when those capabilities were available or planned for availability.
-  Highlights select additional capability present in GitLab, that is not yet reflected in the GitHub's roadmap.

For the full details on GitLab roadmap please visit the [GitLab Directions](/direction/) page.  GitLab also measures its overall maturity against various DevOps Stages.  This self-assessment can be found here in the [Product Maturity](/direction/maturity/) page.


# CI-CD Roadmap Comparison


![Roadmap Comparison 1 of 2](/devops-tools/github/images/ci-cd-roadmap1.png)
![Roadmap Comparison 2 of 2](/devops-tools/github/images/ci-cd-roadmap2.png)
